import React ,{Fragment} from 'react';

import Ingred from'../components/ingredient';

import './burger.css';

const Burger = props =>
       <Fragment>
           <div className="Price">Total price: {props.total} som </div>
            <div className="Burger">Burger
                <div className="BreadTop">
                    <div className="Seeds1"></div>
                    <div className="Seeds2"></div>
                </div>
                    {props.ingredients.map((name,index)=>{
                        return(
                            <Ingred
                            key={index} name={name.name} count={name.count}
                            />
                        )
                    })
                    }
                <div className="BreadBottom"></div>
            </div>
          </Fragment>

 export default Burger;

