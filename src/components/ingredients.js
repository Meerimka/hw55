import React from 'react';
import './burger.css';

const  Ingredients = props => (
            <div className="Ingredients">
                <button onClick={()=> props.addIngredient(props.ingredient)}>
                    <img className="imgImages" src={props.ingredient.image}/>
                </button>
                <span>{props.ingredient.name+" "}</span>
                <span>{props.ingredient.count}x</span>
                <span>{" " +props.ingredient.price}сом</span>
                {props.ingredient.count !== 0 ? <button onClick ={()=> props.remove(props.ingredient)}>-</button> : null}
            </div>
);

export default Ingredients;


