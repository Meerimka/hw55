import React, { Component } from 'react';
import './App.css';
import Burger from "./components/burger";
import Ingredients from './components/ingredients';

import meatImg from './assets/meat.png';
import cheeseImg from './assets/cheese.png';
import saladImg from './assets/salad.png';
import baconImg from './assets/bacon.png';


class App extends Component {
    state = {
        ingredients: [
            {name: 'Meat', count: 0, price: 50, image: meatImg },
            {name: 'Cheese', count: 0, price: 20, image: cheeseImg },
            {name: 'Salad', count: 0, price: 5, image: saladImg },
            {name: 'Bacon', count: 0, price: 30, image: baconImg }
        ],
        showIngredients:false,
        totalPrice: 20
};
     removeIngredient = ingredient => {
        const ingredients = [...this.state.ingredients];
        for(let i=0; i<ingredients.length; i++){
            if(ingredients[i].name=== ingredient.name && ingredients[i].count !== 0 ){
                ingredients[i].count--;
            }
        }
         let totalPrice = this.state.totalPrice;
        if(totalPrice > 20){
            totalPrice -= ingredient.price;
        }else {
            alert('No money yet!')
        }


        this.setState({ingredients, totalPrice});
    };
    addIngredient = ingredient => {
        const ingredients = [...this.state.ingredients];
        for(let i=0; i<ingredients.length; i++){
            if(ingredients[i].name=== ingredient.name){
                ingredients[i].count++;
            }
        }
        let totalPrice = this.state.totalPrice;
        totalPrice += ingredient.price;
        this.setState({ingredients, totalPrice});
    };
    toggleIngredients = () =>{
        this.setState({
            showIngredients: !this.state.showIngredients
        });
    };
    totalPrice = () =>{
        const ingredients = [...this.state.ingredients];
        let totalPrice = 0;
        for(let i=0; i<ingredients.length; i++){
            totalPrice +=(ingredients[i].price * ingredients[i].count);
        }
        this.setState({totalPrice});

    };
    render() {
        let ingredients = null;
        if (this.state.showIngredients) {
            ingredients =this.state.ingredients.map((ingredient, index) => {
                return (
                    <Ingredients
                        ingredient={ingredient}
                        remove={this.removeIngredient}
                        addIngredient={this.addIngredient}
                    >
                    </Ingredients>
                );
            });

        }
        return (
            <div className="App">
                <div>
                    <button className="btnToggle" onClick={this.toggleIngredients}>Are you hungry?</button>
                </div>
                {ingredients}
              <Burger
                  total = {this.state.totalPrice}
                  ingredients={this.state.ingredients}
              />
            </div>
        );
    }
}

export default App;
